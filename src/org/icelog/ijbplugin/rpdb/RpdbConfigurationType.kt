package org.icelog.ijbplugin.rpdb

import com.intellij.diagnostic.VMOptions
import com.intellij.execution.configurations.ConfigurationFactory
import com.intellij.execution.configurations.ConfigurationType
import com.intellij.execution.configurations.RunConfiguration
import com.intellij.openapi.application.PathManager
import com.intellij.openapi.project.Project
import com.intellij.openapi.util.IconLoader
import com.intellij.openapi.util.io.FileUtil
import java.io.File
import java.io.IOException
import javax.swing.Icon

/**
 * Copyright 2016 Ivan Usalko (Usalko.com)
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
class RpdbConfigurationType : ConfigurationType {

    var VM_PARAMETERS: String? = null
    //var PROGRAM_PARAMETERS: String

    val factory = object : ConfigurationFactory(this) {
        override fun createTemplateConfiguration(project: Project): RunConfiguration {
            val runConfiguration = RpdbRunConfiguration(project, this, "")
            if (VM_PARAMETERS == null) {
                VM_PARAMETERS = vmParameters
            } else {
                VM_PARAMETERS += vmParameters
            }
            return runConfiguration
        }
    };

    override fun getIcon(): Icon? {
        return IconLoader.getIcon("/icons/icon2.png")
    }

    override fun getConfigurationTypeDescription(): String? {
        return RpdbBundle.message("run.configuration.type.description")
    }

    override fun getId(): String {
        return "#org.icelog.ijbplugin.rpdb.RpdbConfigurationType"
    }

    override fun getDisplayName(): String? {
        return RpdbBundle.message("run.configuration.title")
    }

    override fun getConfigurationFactories(): Array<out ConfigurationFactory>? {
        return arrayOf(factory)
    }

    val vmParameters: String = ""

    //    val vmParameters: String by lazy {
//        val vmOptions: String?
//        try {
//            vmOptions = FileUtil.loadFile(File(PathManager.getBinPath(), "idea.plugins.vmoptions"))
//        } catch (e: IOException) {
//            vmOptions = VMOptions.read()
//        }
//
//        return@lazy if (vmOptions != null) vmOptions.replace("\\s+".toRegex(), " ").trim { it <= ' ' } else ""
//    }

}

