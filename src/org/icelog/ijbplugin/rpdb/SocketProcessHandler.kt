package org.icelog.ijbplugin.rpdb

import com.intellij.execution.process.ProcessAdapter
import com.intellij.execution.process.ProcessEvent
import com.intellij.execution.process.ProcessHandler
import com.intellij.execution.process.ProcessOutputTypes
import com.intellij.openapi.application.ApplicationManager
import com.intellij.util.io.BaseOutputReader
import java.io.InputStream
import java.io.OutputStream
import java.net.Socket
import java.util.concurrent.Future

/**
 * Copyright 2016 Ivan Usalko (Usalko.com)
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
internal class SocketProcessHandler(private val mySocket: Socket, private val myCommandLine: String) : ProcessHandler() {
    private var myOutputReader: SocketOutputReader? = null

    public override fun startNotify() {
        notifyTextAvailable(myCommandLine + '\n', ProcessOutputTypes.SYSTEM)

        addProcessListener(object : ProcessAdapter() {
            public override fun startNotified(event: ProcessEvent?) {
                try {
                    myOutputReader = SocketOutputReader(mySocket.getInputStream())
                } catch (e: Exception) {
                    //pass
                } finally {
                    removeProcessListener(this)
                }
            }
        })

        super.startNotify()
    }

    public override fun isProcessTerminated(): Boolean {
        return mySocket.isClosed()
    }

    protected override fun destroyProcessImpl() {
        try {
            mySocket.close()
            myOutputReader!!.stop()
        } catch (e: Exception) {
            //pass
        }

    }

    protected override fun detachProcessImpl() {
        destroyProcess()
    }

    public override fun detachIsDefault(): Boolean {
        return false
    }

    public override fun getProcessInput(): OutputStream? {
        return null
    }

    private inner class SocketOutputReader(inputStream: InputStream) : BaseOutputReader(inputStream, null) {
        init {
            start(myCommandLine)
        }

        protected override fun executeOnPooledThread(runnable: Runnable): Future<*> {
            return ApplicationManager.getApplication().executeOnPooledThread(runnable)
        }

        protected override fun onTextAvailable(text: String) {
            if (text == "\u0000\u001A\n") {
                destroyProcess()
            } else {
                notifyTextAvailable(text, ProcessOutputTypes.STDOUT)
            }
        }
    }

    private fun start(myCommandLine: String) {
    }
}

/*
class SocketProcessHandler extends ProcessHandler {
  @NotNull private final String myCommandLine;
  private final Socket mySocket;
  private SocketOutputReader myOutputReader;

  SocketProcessHandler(Socket socket, @NotNull String commandLine) {
    mySocket = socket;
    myCommandLine = commandLine;
  }

  @Override
  public void startNotify() {
    notifyTextAvailable(myCommandLine + '\n', ProcessOutputTypes.SYSTEM);

    addProcessListener(new ProcessAdapter() {
      @Override
      public void startNotified(final ProcessEvent event) {
        try {
          myOutputReader = new SocketOutputReader(mySocket.getInputStream());
        }
        catch (Exception e) {
          //pass
        }
        finally {
          removeProcessListener(this);
        }
      }
    });

    super.startNotify();
  }

  @Override
  public boolean isProcessTerminated() {
    return mySocket.isClosed();
  }

  @Override
  protected void destroyProcessImpl() {
    try {
      mySocket.close();
      myOutputReader.stop();
    }
    catch (Exception e) {
      //pass
    }
  }

  @Override
  protected void detachProcessImpl() {
    destroyProcess();
  }

  @Override
  public boolean detachIsDefault() {
    return false;
  }

  @Nullable
  @Override
  public OutputStream getProcessInput() {
    return null;
  }

  private class SocketOutputReader extends BaseOutputReader {
    public SocketOutputReader(InputStream inputStream) {
      super(inputStream, null);
      start(myCommandLine);
    }

    @NotNull
    @Override
    protected Future<?> executeOnPooledThread(@NotNull Runnable runnable) {
      return ApplicationManager.getApplication().executeOnPooledThread(runnable);
    }

    @Override
    protected void onTextAvailable(@NotNull String text) {
      if (text.equals("\u0000\u001A\n")) {
        destroyProcess();
      }
      else {
        notifyTextAvailable(text, ProcessOutputTypes.STDOUT);
      }
    }
  }
}
 */