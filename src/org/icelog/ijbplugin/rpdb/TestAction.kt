package org.icelog.ijbplugin.rpdb

import com.intellij.openapi.actionSystem.ActionPlaces
import com.intellij.openapi.actionSystem.AnAction
import com.intellij.openapi.actionSystem.AnActionEvent
import com.intellij.openapi.actionSystem.Presentation

/**
 * Copyright 2016 Ivan Usalko (Usalko.com)
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
class TestAction() : AnAction() {
    override fun actionPerformed(e: AnActionEvent?) {
        //Nothing so an it's a test action
    }

    override fun update(e: AnActionEvent?) {
        var presentation : Presentation = e!!.presentation
        if (e.place.equals(ActionPlaces.MAIN_MENU)) {
            presentation.text = "Test action item name"
        } else if (e.place.equals(ActionPlaces.MAIN_TOOLBAR)) {
            presentation.text = "Test action item name"
        }
    }
}