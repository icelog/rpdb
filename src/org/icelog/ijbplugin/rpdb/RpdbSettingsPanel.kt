package org.icelog.ijbplugin.rpdb

import com.intellij.openapi.util.text.StringUtil
import javax.swing.JComponent
import javax.swing.JPanel
import javax.swing.JTextField

/**
 * Copyright 2016 Ivan Usalko (Usalko.com)
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

class RpdbSettingsPanel {
    private val pythonCommandPort: JTextField? = null
    private val panel: JPanel? = null
    private var settingsProvider: RpdbSettingsProvider

    constructor(provider: RpdbSettingsProvider): super() {
        settingsProvider = provider
        reset()
    }

    fun createPanel(): JComponent {
        return panel!!
    }

    fun isModified(): Boolean {
        return getPythonCommandPort() != settingsProvider.port
    }

    fun getPythonCommandPort(): Int {
        return StringUtil.parseInt(pythonCommandPort!!.text, -1)
    }

    fun apply() {
        settingsProvider.port = getPythonCommandPort()
    }

    fun reset() {
        setPythonCommandPort(settingsProvider.port)
    }

    fun setPythonCommandPort(port: Int) {
        pythonCommandPort!!.text = Integer.toString(port)
    }

}
