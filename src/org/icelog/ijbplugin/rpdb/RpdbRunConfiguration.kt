package org.icelog.ijbplugin.rpdb

import com.intellij.execution.Executor
import com.intellij.execution.configurations.*
import com.intellij.execution.runners.ExecutionEnvironment
import com.intellij.openapi.module.Module
import com.intellij.openapi.options.SettingsEditor
import com.intellij.openapi.project.Project

/**
 * Copyright 2016 Ivan Usalko (Usalko.com)
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

class RpdbRunConfiguration : RunConfigurationBase, ModuleRunConfiguration {

    constructor(project: Project, factory: ConfigurationFactory, name: String?) : super(project, factory, name) {
        System.out.println("OK")
    }

    override fun checkConfiguration() {
        throw UnsupportedOperationException()
    }

    override fun getConfigurationEditor(): SettingsEditor<out RunConfiguration> {
        throw UnsupportedOperationException()
    }

    override fun getState(executor: Executor, environment: ExecutionEnvironment): RunProfileState? {
        throw UnsupportedOperationException()
    }

    override fun getModules(): Array<out Module> {
        throw UnsupportedOperationException()
    }

}
