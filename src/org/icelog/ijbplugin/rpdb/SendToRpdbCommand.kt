package org.icelog.ijbplugin.rpdb

import com.intellij.execution.ExecutionException
import com.intellij.execution.RunContentExecutor
import com.intellij.execution.process.ProcessHandler
import com.intellij.execution.process.ProcessOutputTypes
import com.intellij.openapi.diagnostic.Logger
import com.intellij.openapi.project.Project
import com.intellij.openapi.ui.Messages
import com.intellij.openapi.util.Computable
import com.intellij.openapi.util.text.StringUtil
import com.intellij.openapi.vfs.VirtualFile
import java.io.BufferedOutputStream
import java.io.PrintWriter
import java.net.Socket

/**
 * Copyright 2016 Ivan Usalko (Usalko.com)
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
class SendToRpdbCommand(private val myProject: Project, private val myPythonCommandPort: Int) {
    private var myScriptText: String? = null
    private var myFile: VirtualFile? = null

    fun run() {
        try {
            val process = createRunInMayaProcessHandler()

            RunContentExecutor(myProject, process).withTitle(title).withRerun { this@SendToRpdbCommand.run() }.withStop({ process.destroyProcess() }) { !process.isProcessTerminated }.run()
        } catch (e: ExecutionException) {
            Messages.showErrorDialog(myProject, e.message, title)
        }

    }

    //@Throws(ExecutionException::class)
    private fun createRunInMayaProcessHandler(): ProcessHandler {


        try {
            val socket = Socket("127.0.0.1", myPythonCommandPort)
            val processHandler = SocketProcessHandler(socket, title)
            try {
                val writer = PrintWriter(BufferedOutputStream(socket.outputStream))

                if (myScriptText != null) {
                    val lines = scriptLines
                    writeLines(writer, lines)

                    processHandler.notifyTextAvailable(
                            "Sent " + lines.size + " line" + (if (lines.size != 1) "s" else "") + " to command port " + myPythonCommandPort + "\n",
                            ProcessOutputTypes.SYSTEM)
                } else {
                    writeFile(writer, myFile!!)
                    processHandler.notifyTextAvailable(
                            "Sent " + myFile!!.path + " to command port " + myPythonCommandPort + "\n", ProcessOutputTypes.SYSTEM)
                }

                writer.flush()
            } catch (e: Exception) {
                if (!socket.isClosed) {
                    socket.close()
                }
                throw ExecutionException(e.message)
            }

            return processHandler
        } catch (e: Exception) {
            throw ExecutionException(e.message)
        }

    }

    val scriptLines: Array<String>
        get() = StringUtil.splitByLines(myScriptText!!)

    val title: String
        get() = "Send to Maya"

    fun withSelectionText(selectionText: String): SendToRpdbCommand {
        myScriptText = selectionText
        return this
    }

    fun withFile(file: VirtualFile): SendToRpdbCommand {
        myFile = file
        return this
    }

    companion object {
        //        private val LOG = Logger.getInstance(SendToRpdbCommand::class.java)
        private val LOG = Logger.getInstance("SendToRpdbCommand")//javaClass<SendToRpdbCommand>())

        private val PY_CMD_TEMPLATE = "import traceback\nimport sys\nimport __main__\ntry:\n\texec('''%s''', __main__.__dict__, __main__.__dict__)\nexcept:\n\ttraceback.print_exc()\nsys.stdout.write(%s)"

        private val PY_FILE_TEMPLATE = "import traceback\nimport sys\nimport __main__\ntry:\n\texecfile('''%s''', __main__.__dict__, __main__.__dict__)\nexcept:\n\ttraceback.print_exc()\nsys.stdout.write(%s)"

        private val TERMINATION_STRING = "'%c\\n'%26"

        private fun writeLines(writer: PrintWriter, lines: Array<String>) {
            writer.print(java.lang.String.format(PY_CMD_TEMPLATE, StringUtil.join(lines, "\n"), TERMINATION_STRING))
        }

        private fun writeFile(writer: PrintWriter, file: VirtualFile) {
            writer.print(java.lang.String.format(PY_FILE_TEMPLATE, file.path, TERMINATION_STRING))
        }
    }
}

/*
public class SendToMayaCommand {
  private static final Logger LOG = Logger.getInstance(SendToMayaCommand.class);

  private Project myProject;
  private String myScriptText = null;
  private VirtualFile myFile = null;

  private final int myPythonCommandPort;

  private final static String PY_CMD_TEMPLATE =
    "import traceback\nimport sys\nimport __main__\ntry:\n\texec('''%s''', __main__.__dict__, __main__.__dict__)\nexcept:\n\ttraceback.print_exc()\nsys.stdout.write(%s)";

  private final static String PY_FILE_TEMPLATE =
    "import traceback\nimport sys\nimport __main__\ntry:\n\texecfile('''%s''', __main__.__dict__, __main__.__dict__)\nexcept:\n\ttraceback.print_exc()\nsys.stdout.write(%s)";

  private final static String TERMINATION_STRING = "'%c\\n'%26";


  public SendToMayaCommand(Project project, int port) {
    myProject = project;
    myPythonCommandPort = port;
  }

  public void run() {
    try {
      final ProcessHandler process = createRunInMayaProcessHandler();

      new RunContentExecutor(myProject, process)
        .withTitle(getTitle())
        .withRerun(new Runnable() {
          @Override
          public void run() {
            SendToMayaCommand.this.run();
          }
        })
        .withStop(new Runnable() {
                    @Override
                    public void run() {
                      process.destroyProcess();
                    }
                  }, new Computable<Boolean>() {

                    @Override
                    public Boolean compute() {
                      return !process.isProcessTerminated();
                    }
                  }
        )
        .run();
    }
    catch (ExecutionException e) {
      Messages.showErrorDialog(myProject, e.getMessage(), getTitle());
    }
  }

  private ProcessHandler createRunInMayaProcessHandler() throws ExecutionException {


    try {
      final Socket socket = new Socket("127.0.0.1", myPythonCommandPort);
      final SocketProcessHandler processHandler = new SocketProcessHandler(socket, getTitle());
      try {
        PrintWriter writer = new PrintWriter(new BufferedOutputStream(socket.getOutputStream()));

        if (myScriptText != null) {
          String[] lines = getScriptLines();
          writeLines(writer, lines);

          processHandler.notifyTextAvailable(
            "Sent " + lines.length + " line" + (lines.length != 1 ? "s" : "") + " to command port " + myPythonCommandPort + "\n",
            ProcessOutputTypes.SYSTEM);
        }
        else {
          writeFile(writer, myFile);
          processHandler.notifyTextAvailable(
            "Sent " + myFile.getPath() + " to command port " + myPythonCommandPort + "\n", ProcessOutputTypes.SYSTEM);
        }

        writer.flush();
      }
      catch (Exception e) {
        if (!socket.isClosed()) {
          socket.close();
        }
        throw new ExecutionException(e.getMessage());
      }

      return processHandler;
    }
    catch (Exception e) {
      throw new ExecutionException(e.getMessage());
    }
  }

  private static void writeLines(PrintWriter writer, String[] lines) {
    writer.print(String.format(PY_CMD_TEMPLATE, StringUtil.join(lines, "\n"), TERMINATION_STRING));
  }

  private static void writeFile(PrintWriter writer, VirtualFile file) {
    writer.print(String.format(PY_FILE_TEMPLATE, file.getPath(), TERMINATION_STRING));
  }

  public String[] getScriptLines() {
    return StringUtil.splitByLines(myScriptText);
  }

  public String getTitle() {
    return "Send to Maya";
  }

  public SendToMayaCommand withSelectionText(String selectionText) {
    myScriptText = selectionText;
    return this;
  }

  public SendToMayaCommand withFile(VirtualFile file) {
    myFile = file;
    return this;
  }
}
 */