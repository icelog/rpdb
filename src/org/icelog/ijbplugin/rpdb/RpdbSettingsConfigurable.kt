package org.icelog.ijbplugin.rpdb

import com.intellij.openapi.Disposable
import com.intellij.openapi.options.Configurable
import com.intellij.openapi.options.ConfigurationException
import com.intellij.openapi.options.SearchableConfigurable
import com.intellij.openapi.project.Project
import com.intellij.openapi.util.Disposer
import org.jetbrains.annotations.Nls
import javax.swing.JComponent

/**
 * Copyright 2016 Ivan Usalko (Usalko.com)
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
class RpdbSettingsConfigurable(private val myProject: Project) : SearchableConfigurable, Configurable.NoScroll, Disposable {

    private var myPanel: RpdbSettingsPanel? = null

    private val mySettingsProvider: RpdbSettingsProvider

    init {
        mySettingsProvider = RpdbSettingsProvider.getInstance(myProject)
    }

    override fun getId(): String {
        return "rpdb"
    }

    override fun enableSearch(option: String): Runnable? {
        return null
    }


    @Nls
    override fun getDisplayName(): String {
        return "Rpdb"
    }

    override fun getHelpTopic(): String? {
        return CONSOLE_SETTINGS_HELP_REFERENCE
    }

    override fun createComponent(): JComponent? {
        myPanel = RpdbSettingsPanel(mySettingsProvider)

        return myPanel!!.createPanel()
    }

    override fun isModified(): Boolean {
        return myPanel!!.isModified()
    }

    //@Throws(ConfigurationException::class)
    override fun apply() {
        myPanel!!.apply()
    }


    override fun reset() {
        myPanel!!.reset()
    }

    override fun disposeUIResources() {
        Disposer.dispose(this)
    }

    override fun dispose() {
        myPanel = null
    }

    companion object {
        val CONSOLE_SETTINGS_HELP_REFERENCE = "reference.settings.ssh.terminal"
    }
}

/*
public class MayaSettingsConfigurable implements SearchableConfigurable, Configurable.NoScroll, Disposable {
  public static final String CONSOLE_SETTINGS_HELP_REFERENCE = "reference.settings.ssh.terminal";

  private MayaSettingsPanel myPanel;

  private final MayaSettingsProvider mySettingsProvider;
  private Project myProject;

  public MayaSettingsConfigurable(Project project) {
    mySettingsProvider = MayaSettingsProvider.getInstance(project);
    myProject = project;
  }

  @NotNull
  @Override
  public String getId() {
    return "maya";
  }

  @Override
  public Runnable enableSearch(String option) {
    return null;
  }


  @Nls
  @Override
  public String getDisplayName() {
    return "Maya";
  }

  @Override
  public String getHelpTopic() {
    return CONSOLE_SETTINGS_HELP_REFERENCE;
  }

  @Override
  public JComponent createComponent() {
    myPanel = new MayaSettingsPanel(mySettingsProvider);

    return myPanel.createPanel();
  }

  @Override
  public boolean isModified() {
    return myPanel.isModified();
  }

  @Override
  public void apply() throws ConfigurationException {
    myPanel.apply();
  }


  @Override
  public void reset() {
    myPanel.reset();
  }

  @Override
  public void disposeUIResources() {
    Disposer.dispose(this);
  }

  @Override
  public void dispose() {
    myPanel = null;
  }
}
 */