package org.icelog.ijbplugin.rpdb

import com.intellij.openapi.components.*
import com.intellij.openapi.project.Project

/**
 * Copyright 2016 Ivan Usalko (Usalko.com)
 * <p/>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p/>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p/>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
//@State(
//        name = "RpdbSettingsProvider",
//        storages = arrayOf(@Storage(file = StoragePathMacros.WORKSPACE_FILE))
//)
class RpdbSettingsProvider : PersistentStateComponent<RpdbSettingsProvider.State> {
    private val myState = State()

    var port: Int
        get() = myState.myPort
        set(port) {
            myState.myPort = port
        }

    override fun getState(): State? {
        return myState
    }

    override fun loadState(state: State) {
        myState.myPort = state.myPort
    }

    class State {
        var myPort: Int = 0
    }

    companion object {

        fun getInstance(project: Project): RpdbSettingsProvider {
            //return ServiceManager.getService(project, RpdbSettingsProvider::class.java)
            return ServiceManager.getService(project, javaClass<RpdbSettingsProvider>())
        }
    }
}

/*
@State(
  name = "MayaSettingsProvider",
  storages = {
    @Storage(file = StoragePathMacros.WORKSPACE_FILE)
  }
)
public class MayaSettingsProvider implements PersistentStateComponent<MayaSettingsProvider.State> {
  private State myState = new State();

  public void setPort(int port) {
    myState.myPort = port;
  }

  public int getPort() {
    return myState.myPort;
  }

  public static MayaSettingsProvider getInstance(Project project) {
    return ServiceManager.getService(project, MayaSettingsProvider.class);
  }

  @Override
  public State getState() {
    return myState;
  }

  @Override
  public void loadState(State state) {
    myState.myPort = state.myPort;
  }

  public static class State {
    public int myPort;
  }
}

 */